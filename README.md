**Change of plans: instead of starting a new application from scratch, we'll be [helping Whisperfish implement a Kirigami GUI](https://gitlab.com/rubdos/whisperfish/-/issues/318).**

# Flare

Flare is an UNOFFICIAL client for [Signal](https://signal.org/) using Qt Quick. It uses the [libsignal-client](https://github.com/signalapp/libsignal-client)
Rust library but is not affiliated with nor endorsed by Signal Messenger, LLC.

The primary platform target for this is Linux smartphones, but other platforms might be added in the future if you step up to maintain them.

# Status & Roadmap

  1. [ ] Device registration
  2. [ ] Sending and receiving text messages
  3. [ ] Encrypted database
  4. [ ] Import contacts
  5. [ ] Use the WebSocket API for notifications like the Android client does on Android with neither Google Play nor microG
  6. [ ] File attachments
  7. [ ] Voice calls
  8. [ ] Video calls
  9. [ ] Device linking
  10. [ ] Linux daemon to receive Firebase Cloud Messenging push notifications
  11. [ ] SMS support
  12. [ ] MMS support
  13. [ ] Streamline device registration experience by automatically handling confirmation SMS
